#!/bin/bash

# Variables populated by Terraform template
db_username=${db_username}
db_user_password=${db_user_password}
db_name=${db_name}
db_RDS=${db_RDS}

# Install LAMP Server
sudo yum update -y
sudo yum install -



yfsql
;;;';;;;;;;;.'
# Enable PHP 7.xx from Amazon Linux Extras and install it
sudo amazon-linux-extras enable php7.4
sudo yum clean metadata
sudo yum install -y php php-{pear,cgi,common,curl,mbstring,gd,mysqlnd,gettext,bcmath,json,xml,fpm,intl,zip,imap,devel}

# Install Imagick extension
sudo yum -y install gcc ImageMagick ImageMagick-devel ImageMagick-perl
sudo pecl install imagick
sudo chmod 755 /usr/lib64/php/modules/imagick.so
sudo tee -a /etc/php.d/20-imagick.ini <<EOF
extension=imagick
EOF
sudo systemctl restart php-fpm.service

# Start Apache
sudo systemctl start httpd

# Change OWNER and permission of directory /var/www
sudo usermod -a -G apache ec2-user
sudo chown -R ec2-user:apache /var/www
sudo find /var/www -type d -exec chmod 2775 {} \;
sudo find /var/www -type f -exec chmod 0664 {} \;

# Install MariaDB server and start it
sudo yum -y install mariadb-server
sudo systemctl start mariadb

# Install WordPress using WP CLI
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
wp core download --path=/var/www/html --allow-root
wp config create --dbname=$db_name --dbuser=$db_username --dbpass=$db_user_password --dbhost=$db_RDS --path=/var/www/html --allow-root --extra-php <<PHP
define( 'FS_METHOD', 'direct' );
define('WP_MEMORY_LIMIT', '128M');
PHP

# Change permission of /var/www/html/
sudo chown -R ec2-user:apache /var/www/html
sudo chmod -R 774 /var/www/html

# Enable .htaccess files in Apache config
sudo sed -i '/<Directory "\/var\/www\/html">/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/httpd/conf/httpd.conf

# Make Apache autostart and restart Apache
sudo systemctl enable httpd.service
sudo systemctl restart httpd.service

echo WordPress Installed
