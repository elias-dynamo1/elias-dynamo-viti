terraform {
  backend "s3" {
    bucket = "teraformstatebucket1234"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}

provider "aws" {
  region = "us-east-1"
}

variable "database_user" {
  description = "Username for the database"
}

variable "database_password" {
  description = "Password for the database"
}

variable "database_name" {
  description = "Name of the database"
}

# Data source to render user data script file
data "template_file" "user_data" {
  template = file("${path.module}/userdata.sh")

  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.terraform-RDS.endpoint
  }
}

# EC2 Instance
resource "aws_instance" "terraform-instance" {
  ami           = "ami-0a699202e5027c10d"
  instance_type = "t2.micro"
  
  # Reference the existing VPC ID and subnet ID
  subnet_id     = "subnet-08fbbc8819e0dcf3b"
  
  # Allocate a public IP address for the instance
  associate_public_ip_address = true
  iam_instance_profile        = "terraform-Role"
  
  # Add the specified security group to the instance
  security_groups = ["sg-052c6da5dac3e3fd4"]
  
  user_data = data.template_file.user_data.rendered
}

# Create a DB subnet group
resource "aws_db_subnet_group" "example" {
  name       = "terraform-db-subnet-group"
  subnet_ids = ["subnet-03be6bc367413cd9e", "subnet-08fbbc8819e0dcf3b"]
}
  
# RDS MySQL Database
resource "aws_db_instance" "terraform-RDS" {
  instance_class          = "db.t3.micro"
  engine                  = "mysql"
  engine_version          = "8.0.35"
  identifier              = "terraform-db"
  username                = "admin"
  password                = "Abcd1234!"
  publicly_accessible     = false
  multi_az                = false
  skip_final_snapshot     = true
  
  # Specify the subnet group
  db_subnet_group_name    = aws_db_subnet_group.example.name
  
  # Specify the existing security group
  vpc_security_group_ids  = ["sg-052c6da5dac3e3fd4"]
  
  # Other configuration options as needed
  
  # Ensure that the RDS instance falls within the Free Tier limits
  allocated_storage       = 20  # GB, within the Free Tier limit
  max_allocated_storage   = 20  # GB, within the Free Tier limit
  storage_type            = "gp2"  # General Purpose (SSD), within the Free Tier limit
}

# Outputs
output "terraform_instance_public_ip" {
  value = aws_instance.terraform-instance.public_ip
}

output "terraform_RDS_endpoint" {
  value = aws_db_instance.terraform-RDS.endpoint
}
